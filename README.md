==============
	eArt
==============
 
eArt Tool is basically designed for run the test case and update the report in eKworld.

Step-1
	Driver should be in following path
	
		D:\32IEDriverServer.exe

Step-2
	eJs file and jQuery library must be in following path
	
		D:\eArt\eJs.js
		
		D:\eArt\jquery1_12_4.min.js
		
		
Step-3
	Above the steps completed you are ready for execute the jar file.
	
		1.	Open the command Prompt.
		2.	Then Navigate to where you having jar file
		3.	Enter the following command
			Java –jar eArt.jar TestCasefolderPath Username Password
			Ex:java –jar eArt.jar D:\ekWorld_Demo\Sprint8\TestCases demo@esolveindia.com 12345
		4.	After the executed results are updated in eKworld
			Note: Username and Passwords are must be eKworld user details.

